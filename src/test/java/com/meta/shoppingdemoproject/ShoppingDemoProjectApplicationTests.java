package com.meta.shoppingdemoproject;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


class ShoppingDemoProjectApplicationTests {

    private Calculator calculator = new Calculator();

    @Test
    @Disabled
    void contextLoads() {
        System.out.println("hello");
    }

    @Test
    void testSum() {
        int expectedResult = 21;
        //calculated result
        int result = calculator.sum(3, 12, 6);

        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testProduct(){
        assertThat(calculator.product(2,3)).isEqualTo(6);
    }

}
