package com.meta.shoppingdemoproject.controller;

import com.meta.shoppingdemoproject.dto.GlobalAPIResponse;
import com.meta.shoppingdemoproject.dto.ProductDto;
import com.meta.shoppingdemoproject.dto.UserRequestDto;
import com.meta.shoppingdemoproject.service.productservice.ProductService;
import com.meta.shoppingdemoproject.service.userservice.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ProductControllerTest {

    @InjectMocks
    private ProductController yourControllerClass;

    @Mock
    private ProductService productService;

    @Mock
    private UserService userService;

    @Test
    public void saveProduct_ValidProduct_ReturnsOk() {

        Integer userId=userService.createUser(new UserRequestDto(null,"Kathmandu","9874563210","apurba thapa","apurba1213"));

        // Arrange
        ProductDto validProductDto = new ProductDto(null, "Laptop", "Used by developers", 20000D, 45, userId);

        // Act
        ResponseEntity<GlobalAPIResponse> responseEntity = yourControllerClass.saveProduct(validProductDto);

        // Assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("Product saved successfully", responseEntity.getBody().getMessage());
        assertEquals(true, responseEntity.getBody().getStatus());
        assertEquals(null, responseEntity.getBody().getData());

        // Verify that the saveProduct method of the productService is called with the correct argument.
        verify(productService).saveProduct(validProductDto);
    }
}