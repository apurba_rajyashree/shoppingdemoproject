package com.meta.shoppingdemoproject.service.productservice;

import com.meta.shoppingdemoproject.dto.ProductDto;
import com.meta.shoppingdemoproject.dto.UserRequestDto;
import com.meta.shoppingdemoproject.model.Product;
import com.meta.shoppingdemoproject.repo.ProductRepo;
import com.meta.shoppingdemoproject.repo.UserRepo;
import com.meta.shoppingdemoproject.service.userservice.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.mockito.Mockito.verify;
@DataJpaTest
class ProductServiceImplTest {

    @Mock
    private ProductRepo productRepo;


    @Autowired
    private UserRepo userRepo;

    private UserServiceImpl userService;

    @BeforeEach
    void setUp() {
        this.productService = new ProductServiceImpl(this.productRepo, userRepo, new ModelMapper());
        this.userService=new UserServiceImpl(userRepo);
    }

    private ProductServiceImpl productService;

    @Test
    void saveProduct() {
        Integer userId=userService.createUser(new UserRequestDto(null,"Kathmandu","9874563210","apurba thapa","apurba1213"));

        ProductDto productDto = new ProductDto(null, "Laptop", "Used by developers", 20000D, 45, userId);
        productService.saveProduct(productDto);

        ArgumentCaptor<Product> productArgumentCaptor = ArgumentCaptor.forClass(Product.class);
        verify(productRepo).save(productArgumentCaptor.capture());
    }

    @Test
    void getAllProducts() {
        productService.getAllProducts();
        verify(productRepo).findAll();
    }
}