package com.meta.shoppingdemoproject.repo;

import com.meta.shoppingdemoproject.model.Product;
import com.meta.shoppingdemoproject.model.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ProductRepoTest {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private ProductRepo productRepo;


    @Test
    void findByProductId() {
        User user = userRepo.findById(1).get();
        Product product = new Product(null, "Iphone 13", "Nice photos", 1200000D, 30, user);
        product = productRepo.save(product);

        Product testingProduct=productRepo.findByProductId(product.getId());
        assertThat(testingProduct).isNotNull();
    }

    @BeforeEach
    void setUp() {
        System.out.println("Good night!!");
    }

    @AfterEach
    void tearDown() {
        productRepo.deleteAll();
    }
}