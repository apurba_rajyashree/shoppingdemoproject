package com.meta.shoppingdemoproject.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "order")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false, foreignKey = @ForeignKey(name = "fk_order_user"))
    private User user;

    @Column(name = "order_confirm_date", nullable = false)
    private LocalDate orderConfirmDate;

    @Column(name = "is_paid", nullable = false)
    private Boolean isPaid;

    @OneToMany(mappedBy = "orderDetail")
    private List<ProductQuantityMap> productQuantityMaps;

}
