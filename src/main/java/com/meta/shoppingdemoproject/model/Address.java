package com.meta.shoppingdemoproject.model;


import com.meta.shoppingdemoproject.enums.AddressType;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "address")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "address_type", nullable = false)
    private AddressType addressType;

    @Column(name = "street", nullable = false)
    private String street;

    @Column(name = "house_number", nullable = false)
    private String houseNumber;

    @ManyToOne
    @JoinColumn(
            name = "user_id", nullable = false,
            foreignKey = @ForeignKey(name = "fk_address_user")
    )
    private User user;

}
