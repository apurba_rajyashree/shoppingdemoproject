package com.meta.shoppingdemoproject.model;

import com.meta.shoppingdemoproject.enums.AddressType;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "document")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "file_path", nullable = false)
    private String filePath;   //  C://Users/me\DesktopBOOTCAMP\32312326356235721.pdf

    @Column(name = "original_name", nullable = false)
    private String originalName;  //  abc.png

    @Column(name = "uuid", nullable = false)
    private String uuid;  // 32312326356235721.pdf

}
