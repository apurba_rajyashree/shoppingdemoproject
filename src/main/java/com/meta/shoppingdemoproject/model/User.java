package com.meta.shoppingdemoproject.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Entity
@Table(name = "master_user", uniqueConstraints = {@UniqueConstraint(name = "uk_masteruser_username", columnNames = "username")})
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq_gen")
    @SequenceGenerator(name = "user_seq_gen", allocationSize = 10, sequenceName = "user_sequence")
    @Column(name = "id")
    private Integer id;

    @Column(name = "contact_number", nullable = false, length = 13)
    private String contactNumber;

    @Column(name = "username", nullable = false, length = 50)
    private String username;

    @Column(name = "password", nullable = false, length = 100)
    private String password;

    @Column(name = "email", nullable = false, length = 100)
    private String email;


}
