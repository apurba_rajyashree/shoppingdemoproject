package com.meta.shoppingdemoproject.model;


import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "education_qualification")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EducationQualification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "degree", nullable = false)
    private String degree;

//    @Column(name = "college_name")
//    private String collegeName;
//
//    @Column(name = "college_email")
//    private String collegeEmail;

    @Column(name = "college")
    private String college;

    @OneToOne(fetch = FetchType.LAZY)
    private User user;

}
