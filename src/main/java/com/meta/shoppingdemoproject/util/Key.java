package com.meta.shoppingdemoproject.util;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Getter
@Setter
@Configuration
@ConfigurationProperties("user.created")
public class Key {

    private String key1;
    private String key2;
    private String key3;
    private List<String> key4;

    @Override
    public String toString() {
        return "Key{" +
                "key1='" + key1 + '\'' +
                ", key2='" + key2 + '\'' +
                ", key3='" + key3 + '\'' +
                ", key4=" + key4 +
                '}';
    }
}
