package com.meta.shoppingdemoproject.util;

import com.google.gson.Gson;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;


@Component
public class JsonConverterUtil {


    public JsonConverterUtil(Gson gson) {
        this.gson = gson;
    }

    /**
     * jackson  --> objectMapper   --> serilize and deserialize (JSON to object and vice versa)
     * <p>
     * gson --> GSON --> serilize and deserialize (JSON(string) to object and vice versa)
     */


    private final Gson gson;


    public String convertObjectToJsonString(Object object) {
        String json = gson.toJson(object);
        return json;
    }

    public Object converJsonStringToObject(String json, Type type) {
        return gson.fromJson(json, type);
    }
}
