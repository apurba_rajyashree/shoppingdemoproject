package com.meta.shoppingdemoproject;

import com.meta.shoppingdemoproject.util.Key;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ShoppingDemoProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShoppingDemoProjectApplication.class, args);
    }


}
