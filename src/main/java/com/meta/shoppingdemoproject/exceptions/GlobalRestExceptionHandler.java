package com.meta.shoppingdemoproject.exceptions;

import com.meta.shoppingdemoproject.dto.GlobalAPIResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalRestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({DataNotFoundException.class})
    public ResponseEntity<Object> handleDataNotFoundException(DataNotFoundException ex) {
        ex.printStackTrace();
        String error = ex.getMessage();
        return new ResponseEntity<>(new GlobalAPIResponse(false, error, null), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        ex.printStackTrace();
        List<String> errors = ex.getBindingResult().getFieldErrors().stream().map(FieldError::getDefaultMessage).collect(Collectors.toList());
        String errorMessage = errors.toString().replace("[", "").replace("]", "");
        return new ResponseEntity<>(new GlobalAPIResponse(false, errorMessage, null), HttpStatus.BAD_REQUEST);
    }


//    @ExceptionHandler({DataIntegrityViolationException.class})
//    public ResponseEntity<Object> handleDataIntegrityViolationException(DataIntegrityViolationException ex){
//
//    }

}
