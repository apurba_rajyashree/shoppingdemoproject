package com.meta.shoppingdemoproject.constants;

import lombok.Getter;

@Getter
public class UserContant {

    public final static String LOCATION="User location";
    public final static String USERNAME="Username";
    public final static String PASSWORD="Password";
}
