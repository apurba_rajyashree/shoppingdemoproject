package com.meta.shoppingdemoproject.controller;

import com.meta.shoppingdemoproject.dto.GlobalAPIResponse;
import com.meta.shoppingdemoproject.dto.UserRequestDto;
import com.meta.shoppingdemoproject.model.User;
import com.meta.shoppingdemoproject.service.userservice.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @PostMapping("/register")
    public ResponseEntity<GlobalAPIResponse> createUser(@Valid @RequestBody UserRequestDto userRequestDto) {
        return ResponseEntity.ok(new GlobalAPIResponse(true, "User saved successfully", userService.createUser(userRequestDto)));
//        return new ResponseEntity<>(userService.createUser(userRequestDto), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<String> updateUser(@Valid @RequestBody UserRequestDto userRequestDto) {
        return ResponseEntity.ok(userService.update(userRequestDto));
    }

    @GetMapping
    public ResponseEntity<GlobalAPIResponse> getAllUsers() {
        return ResponseEntity.ok(new GlobalAPIResponse(true, "User List fetched successfully", userService.findAllUsers()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<GlobalAPIResponse> getUserById(@PathVariable("id") Integer userId) {
        return ResponseEntity.ok(new GlobalAPIResponse(true, "User fetched successfuly", userService.findById(userId)));
    }

    @GetMapping("/get-user")
    public ResponseEntity<GlobalAPIResponse> findUserById(@RequestParam("id") Integer userId) {
        return ResponseEntity.ok(new GlobalAPIResponse(true, "User fetched successfuly", userService.findById(userId)));
    }


    @GetMapping(value = {"/get-user-by/address/{address}/contact", "/get-user-by/address/{address}/contact/{contactNumber}"})
    public ResponseEntity<GlobalAPIResponse> getUserByAddressAndContactNumber(@PathVariable(value = "address") String address, @PathVariable(value = "contactNumber", required = false) String contactNumber) {
        return ResponseEntity.ok(new GlobalAPIResponse(true, "User fetched successfuly", userService.findByAddressAndContactNumber(address, contactNumber)));
    }


    @GetMapping("/get-user-by/address/contact")
    public User findUserByAddressAndContactNumber(@RequestParam(value = "address", required = false) String address12, @RequestParam(value = "contactNumber", required = false) String contactNumber) {
        return userService.findByAddressAndContactNumber(address12, contactNumber);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<GlobalAPIResponse> deleteUserById(@PathVariable("id") Integer id) {
        userService.delete(id);
        return ResponseEntity.ok(new GlobalAPIResponse(true, "User deleted successfully", null));
    }


    @GetMapping("/get-user-by-id/{id}")
    public ResponseEntity<GlobalAPIResponse> findUserResponseDtoById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(new GlobalAPIResponse(true, "User fetched successfully", userService.findByUserId(id)));
    }
}
