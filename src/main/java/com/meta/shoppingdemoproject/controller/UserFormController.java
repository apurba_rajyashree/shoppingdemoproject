package com.meta.shoppingdemoproject.controller;


import com.meta.shoppingdemoproject.dto.*;
import com.meta.shoppingdemoproject.service.userform.UserFormService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/form")
public class UserFormController {

    private final UserFormService userFormService;

    @PostMapping("/register")
    public ResponseEntity<GlobalAPIResponse> createUser(@Valid @RequestBody UserFormDto userFormDto) {
        userFormService.saveForm(userFormDto);
        return ResponseEntity.ok(new GlobalAPIResponse(true, "User form saved successfully", null));
    }

    @GetMapping("/{id}")
    public ResponseEntity<GlobalAPIResponse> findById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(new GlobalAPIResponse(true, "User form fetched successfully", userFormService.findByUserId(id)));
    }


    @PostMapping("/education-qualification")
    public ResponseEntity<GlobalAPIResponse> saveCollegeInfo(@Valid @RequestBody UserEducationDto userEducationDto) {
        userFormService.saveEducationQualification(userEducationDto);
        return ResponseEntity.ok(new GlobalAPIResponse(true, "User Education saved successfully", null));
    }

    @GetMapping("/education-qualification/{id}")
    public ResponseEntity<GlobalAPIResponse> findByEducationId(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(new GlobalAPIResponse(true, "User Education fetched successfully", userFormService.findEducationById(id)));
    }
}