package com.meta.shoppingdemoproject.controller;

import com.meta.shoppingdemoproject.dto.GlobalAPIResponse;
import com.meta.shoppingdemoproject.dto.ProductDto;
import com.meta.shoppingdemoproject.dto.UserRequestDto;
import com.meta.shoppingdemoproject.service.productservice.ProductService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/product")
public class ProductController {

    private final ProductService productService;

    @PostMapping
    public ResponseEntity<GlobalAPIResponse> saveProduct(@Valid @RequestBody ProductDto productDto) {
        productService.saveProduct(productDto);
        return ResponseEntity.ok(new GlobalAPIResponse(true, "Product saved successfully", null));
    }
}
