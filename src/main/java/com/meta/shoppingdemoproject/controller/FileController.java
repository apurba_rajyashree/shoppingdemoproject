package com.meta.shoppingdemoproject.controller;

import com.meta.shoppingdemoproject.dto.GlobalAPIResponse;
import com.meta.shoppingdemoproject.service.fileservice.FileService;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/file")
public class FileController {


    private final FileService fileService;


    @PostMapping("/upload")
    public ResponseEntity<GlobalAPIResponse> fileUpload(@RequestParam("file") MultipartFile file) {
        String filePath = null;
        try {
            filePath = fileService.uploadFile(file);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(new GlobalAPIResponse(false, "File could not be uploaded due to internal server error!!", null), HttpStatusCode.valueOf(500));
        }
        return new ResponseEntity<>(new GlobalAPIResponse(true, "File uploaded successfully!!", filePath), HttpStatusCode.valueOf(200));
    }


    @GetMapping("/download")
    public void fileDownload(@RequestParam("uuid") String uuid, HttpServletResponse response) throws FileNotFoundException {
        fileService.downloadFile(uuid, response);
    }
}
