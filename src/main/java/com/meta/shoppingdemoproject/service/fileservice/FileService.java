package com.meta.shoppingdemoproject.service.fileservice;

import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface FileService {


    String uploadFile(MultipartFile file) throws IOException;

    void downloadFile(String uuid, HttpServletResponse response) throws FileNotFoundException;
}
