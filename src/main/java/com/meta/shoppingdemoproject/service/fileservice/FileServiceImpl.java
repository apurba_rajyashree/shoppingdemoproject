package com.meta.shoppingdemoproject.service.fileservice;

import com.meta.shoppingdemoproject.dto.DocumentResponse;
import com.meta.shoppingdemoproject.exceptions.DataNotFoundException;
import com.meta.shoppingdemoproject.model.Document;
import com.meta.shoppingdemoproject.repo.DocumentRepo;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {

    private final DocumentRepo documentRepo;
    private final ModelMapper modelMapper;

    @Value("${upload.file}")
    private String path;

    @Override
    public String uploadFile(MultipartFile multipartFile) throws IOException {
        String directory = System.getProperty("user.home") + "/" + "Desktop" + "BOOTCAMP";
        File file = new File(directory);
        if (!file.exists()) {
            file.mkdir();
        }

        //abc.png
        String originalFileName = multipartFile.getOriginalFilename();

        //generateRandom UUID
        String randomId = UUID.randomUUID().toString();
        //1212121.21.212121212.12121212121.jpeg

        String newFileName = randomId.concat(originalFileName.substring(originalFileName.lastIndexOf('.')));
        String newPath = directory + "/" + newFileName;

        Files.copy(multipartFile.getInputStream(), Paths.get(newPath));

//        File myFile = new File(newPath);
//        multipartFile.transferTo(myFile);

        Document document = Document.builder()
                .filePath(newPath)
                .originalName(originalFileName)
                .uuid(newFileName)
                .build();

        documentRepo.save(document);
        return newPath;
    }

    @Override
    public void downloadFile(String uuid, HttpServletResponse response) throws FileNotFoundException {
        Document document = documentRepo.findByUuid(uuid);
        DocumentResponse documentResponse = modelMapper.map(document, DocumentResponse.class);
        download(documentResponse, response);
    }


    public void download(DocumentResponse documentResponse, HttpServletResponse response) throws FileNotFoundException {
        if (documentResponse == null) throw new DataNotFoundException("Document Not Found");
        File file = new File(documentResponse.getFilePath());
        if (file.exists()) {
            //mimeType is the type of the file . For example : images/jpg   application/pdf
            String mimeType = URLConnection.guessContentTypeFromName(file.getName());
            if (mimeType == null) mimeType = "application/octet-stream";

            response.setContentType(mimeType);
            response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + documentResponse.getOriginalName() + "\"");
            response.setContentLength((int) file.length());

            try (InputStream inputStream = new BufferedInputStream(new FileInputStream(file))){
                FileCopyUtils.copy(inputStream, response.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            throw new FileNotFoundException("File Not found in server");
        }
    }
}
