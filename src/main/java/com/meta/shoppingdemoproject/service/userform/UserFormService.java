package com.meta.shoppingdemoproject.service.userform;

import com.meta.shoppingdemoproject.dto.UserEducationDto;
import com.meta.shoppingdemoproject.dto.UserFormDto;
import com.meta.shoppingdemoproject.dto.UserGeneralInformationDto;

public interface UserFormService {

    void saveForm(UserFormDto userFormDto);

    UserGeneralInformationDto findByUserId(Integer id);

    void saveEducationQualification(UserEducationDto userEducationDto);

    UserEducationDto findEducationById(Integer id);
}
