package com.meta.shoppingdemoproject.service.userform;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meta.shoppingdemoproject.dto.*;
import com.meta.shoppingdemoproject.exceptions.DataNotFoundException;
import com.meta.shoppingdemoproject.model.Address;
import com.meta.shoppingdemoproject.model.EducationQualification;
import com.meta.shoppingdemoproject.model.User;
import com.meta.shoppingdemoproject.repo.AddressRepo;
import com.meta.shoppingdemoproject.repo.EducationRepo;
import com.meta.shoppingdemoproject.repo.UserRepo;
import com.meta.shoppingdemoproject.util.JsonConverterUtil;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserFormServiceImpl implements UserFormService {
    private final UserRepo userRepo;
    private final AddressRepo addressRepo;
    private final EducationRepo educationRepo;

    private final ModelMapper modelMapper;
    private final JsonConverterUtil jsonConverterUtil;

    private static final String DOCUMENT_URL="https:192.168.1.27/document/api/upload-document/final-approved/";

    @Transactional
    @Override
    public void saveForm(UserFormDto userFormDto) {

        //for saving user general information
        UserGeneralInformationDto userGeneralInformationDto = userFormDto.getUserGeneralInformationDto();
        User user = User.builder()
                .id(userGeneralInformationDto.getId() == null ? null : userGeneralInformationDto.getId())
                .username(userGeneralInformationDto.getUserName())
                .password(userGeneralInformationDto.getPassword())
                .contactNumber(userGeneralInformationDto.getContactNumber())
                .email(userGeneralInformationDto.getEmail())
                .build();


        //using model mapper to map userGeneralInformationDto to User.
//        User user = modelMapper.map(userGeneralInformationDto, User.class);
        user = userRepo.save(user);


        //for saving list of address
        List<UserAddressDto> userAddressDtoList = userFormDto.getUserAddressDtoList();

        List<Address> addressList = new ArrayList<>();
        for (UserAddressDto eachAddressDto : userAddressDtoList) {
            Address address = Address.builder()
                    .id(eachAddressDto.getId() == null ? null : eachAddressDto.getId())
                    .addressType(eachAddressDto.getAddressType())
                    .street(eachAddressDto.getStreet())
                    .houseNumber(eachAddressDto.getHouseNumber())
                    .user(user)
                    .build();

            addressList.add(address);
        }
        addressRepo.saveAll(addressList);

        //for saving education qualification
        UserEducationDto userEducationDto = userFormDto.getUserEducationDto();
        EducationQualification educationQualification = EducationQualification.builder()
                .id(userEducationDto.getId() == null ? null : userEducationDto.getId())
                .degree(userEducationDto.getDegree())
//                .collegeEmail(userEducationDto.getCollegeEmail())
//                .collegeName(userEducationDto.getCollegeName())
                .user(user)
                .build();

        educationRepo.save(educationQualification);

    }

    @Override
    public UserGeneralInformationDto findByUserId(Integer id) {
        User user = userRepo.findById(id).orElseThrow(
                () -> new DataNotFoundException("User not found!!")
        );
        return modelMapper.map(user, UserGeneralInformationDto.class);
    }

    @Override
    public void saveEducationQualification(UserEducationDto userEducationDto) {
        User user = userRepo.findById(userEducationDto.getUserId()).orElseThrow(
                () -> new DataNotFoundException("User not found!!")
        );

        EducationQualification educationQualification = EducationQualification.builder()
                .user(user)
                .degree(userEducationDto.getDegree())
                .college(jsonConverterUtil.convertObjectToJsonString(userEducationDto.getCollegeDto()))
                .build();

        educationRepo.save(educationQualification);
    }

    @Override
    public UserEducationDto findEducationById(Integer id) {
        EducationQualification educationQualification = educationRepo.findById(id).orElseThrow(
                () -> new DataNotFoundException("Education not found")
        );

        UserEducationDto userEducationDto = new UserEducationDto();
        userEducationDto.setId(educationQualification.getId());
        userEducationDto.setUserId(educationQualification.getUser().getId());
        userEducationDto.setDegree(educationQualification.getDegree());
        userEducationDto.setCollegeDto((CollegeDto) jsonConverterUtil.converJsonStringToObject(educationQualification.getCollege(), CollegeDto.class));

        return userEducationDto;
    }
}
