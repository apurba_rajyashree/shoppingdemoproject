package com.meta.shoppingdemoproject.service.productservice;

import com.meta.shoppingdemoproject.dto.ProductDto;
import com.meta.shoppingdemoproject.model.Product;

import java.util.List;

public interface ProductService {


    void saveProduct(ProductDto productDto);

    List<Product> getAllProducts();
}
