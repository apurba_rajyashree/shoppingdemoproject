package com.meta.shoppingdemoproject.service.productservice;

import com.meta.shoppingdemoproject.dto.ProductDto;
import com.meta.shoppingdemoproject.model.Product;
import com.meta.shoppingdemoproject.model.User;
import com.meta.shoppingdemoproject.repo.ProductRepo;
import com.meta.shoppingdemoproject.repo.UserRepo;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepo productRepo;

    private final UserRepo userRepo;

    private final ModelMapper modelMapper;

    public ProductServiceImpl(ProductRepo productRepo, UserRepo userRepo, ModelMapper modelMapper) {
        this.productRepo = productRepo;
        this.userRepo = userRepo;
        this.modelMapper = modelMapper;
    }

    @Override
    public void saveProduct(ProductDto productDto) {
        User user = userRepo.findById(productDto.getUserId()).get();
        Product product = modelMapper.map(productDto, Product.class);
        product.setUser(user);
        productRepo.save(product);

    }

    @Override
    public List<Product> getAllProducts() {
        return productRepo.findAll();
    }
}
