package com.meta.shoppingdemoproject.service.userservice;

import com.meta.shoppingdemoproject.dto.UserRequestDto;
import com.meta.shoppingdemoproject.dto.UserResponseDto;
import com.meta.shoppingdemoproject.model.User;

import java.util.List;

public interface UserService {

    Integer createUser(UserRequestDto userRequestDto);

    String update(UserRequestDto userRequestDto);

    User findById(Integer id);


    /**
     * Example for constructor based mapping to UserResponseDto
     * @param id
     * @return
     */
     UserResponseDto findByUserId(Integer id);


        void delete(Integer id);

    User findByAddressAndContactNumber(String address, String contactNumber);


    List<User> findAllUsers();
}
