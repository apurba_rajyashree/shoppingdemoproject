package com.meta.shoppingdemoproject.service.userservice;

import com.meta.shoppingdemoproject.dto.UserRequestDto;
import com.meta.shoppingdemoproject.dto.UserResponseDto;
import com.meta.shoppingdemoproject.exceptions.DataNotFoundException;
import com.meta.shoppingdemoproject.model.User;
import com.meta.shoppingdemoproject.repo.UserRepo;
import jakarta.transaction.Transactional;
import org.aspectj.weaver.World;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

//    @Autowired
//    private UserRepo userRepo;
    private final UserRepo userRepo;

    private static final String DOCUMENT_URL="https:192.168.1.27/document/api/upload-document/";

    public UserServiceImpl(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    private final static String DOES_NOT_EXIST = " does not exist";

    @Transactional
    @Override
    public Integer createUser(UserRequestDto userRequestDto) {


        User user = getUser(userRequestDto);

        user = userRepo.save(user);
        return user.getId();
    }

    private static User getUser(UserRequestDto userRequestDto) {
        User user = User.builder()
//                .address(userRequestDto.getLocation())
                .contactNumber(userRequestDto.getPhoneNumber())
                .username(userRequestDto.getUsername())
                .password(userRequestDto.getPassword())
                .build();
        return user;
    }

    @Override
    public String update(UserRequestDto userRequestDto) {
        User user = userRepo.findById(userRequestDto.getId()).orElseThrow(
                () -> new DataNotFoundException("User id:" + userRequestDto.getId() + DOES_NOT_EXIST)
        );

        user = User.builder()
                .id(user.getId())
                .contactNumber(userRequestDto.getPhoneNumber())
                .username(userRequestDto.getUsername())
                .password(userRequestDto.getPassword())
                .build();

        callDocumentService(DOCUMENT_URL);
        userRepo.save(user);
        return "User updated successfully";
    }

    private void callDocumentService(String documentUrl) {

    }

    @Override
    public User findById(Integer id) {
        return userRepo.findById(id).orElseThrow(
                () -> new DataNotFoundException("User id:" + id + DOES_NOT_EXIST)
        );
    }


    @Override
    public UserResponseDto findByUserId(Integer id) {
        User user= userRepo.findById(id).orElseThrow(
                () -> new DataNotFoundException("User id:" + id + DOES_NOT_EXIST)
        );

        return new UserResponseDto(user);
    }

    @Override
    public void delete(Integer id) {
        userRepo.deleteById(id);
    }

    @Override
    public User findByAddressAndContactNumber(String address, String contactNumber) {
        User user = null;
//        if (contactNumber == null) {
//            user = userRepo.findByAddress(address).orElseThrow(
//                    () -> new DataNotFoundException("User with address: " + address + " not found")
//            );
//        } else {
//            user = userRepo.findByAddressAndContactNumber(address, contactNumber).orElseThrow(
//                    () -> new DataNotFoundException("User with address: " + address + " and contact Number: " + contactNumber + " not found")
//            );
//        }
        return user;
    }

    @Override
    public List<User> findAllUsers() throws NullPointerException, DataNotFoundException {
        return userRepo.findAll();

//        try {
//            return userRepo.findAll();
//            return userRepo.findAll();
//            return userRepo.findAll();
//            return userRepo.findAll();
//            return userRepo.findAll();
//            return userRepo.findAll();
//            return userRepo.findAll();
//            return userRepo.findAll();
//            return userRepo.findAll();
//            return userRepo.findAll();
//            return userRepo.findAll();
//            return userRepo.findAll();
//            return userRepo.findAll();
//            return userRepo.findAll();
//            return userRepo.findAll();
//            return userRepo.findAll();
//            return userRepo.findAll();
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        }catch (ArithmeticException ex){
//
//        } finally {
//            User user = new User();
//        }
//
//        User user1 = new User();
//
//        if (userRepo.findAll().isEmpty()) {
//            throw new DataNotFoundException("User List is Empty");
//            throw new RuntimeException("User List is Empty");
//        }
    }
}
