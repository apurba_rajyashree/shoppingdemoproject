package com.meta.shoppingdemoproject;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
class Laptop {

    private String model;
    private String brand;
    private String price;

    @Override
    public String toString() {
        return "Laptop{" +
                "model='" + model + '\'' +
                ", brand='" + brand + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}


public class ObjectMapperDemo {
    public static void main(String[] args) throws IOException {
        String json = "{\n" +
                "  \"model\": \"test_bf2e53c10c4b\",\n" +
                "  \"brand\": \"test_20e02967c03f\",\n" +
                "  \"price\": \"8900000\",\n" +
                "  \"ram\": \"8gb\"\n" +
                "}";

        ObjectMapper objectMapper = new ObjectMapper();
        Laptop laptop = objectMapper.readValue(json, Laptop.class);
        objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);


        //array
        String arrayJson = "[\n" +
                "    {\n" +
                "        \"model\": \"model 1\",\n" +
                "        \"brand\": \"test_47a92422c817\",\n" +
                "        \"price\": \"test_35ea4ed54cec\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"model\": \"model 2\",\n" +
                "        \"brand\": \"test_47a92422c817\",\n" +
                "        \"price\": \"test_35ea4ed54cec\"\n" +
                "    }\n" +
                "]";

        Laptop[] laptops = objectMapper.readValue(arrayJson, Laptop[].class);
        for (Laptop each : laptops) {
            System.out.println(each.toString());
        }


        //list
        List<Laptop> laptopList = objectMapper.readValue(arrayJson, new TypeReference<List<Laptop>>() {
        });
        System.out.println(laptopList);

        //map
        Map<String, String> laptopMap = objectMapper.readValue(json, new TypeReference<Map<String, String>>() {
        });
        laptopMap.keySet().forEach(key -> {
            System.out.println("Key: " + key);
            System.out.println("Value: " + laptopMap.get(key));
        });

        //Serialize: java object to Json
        Laptop laptop1=new Laptop();
        laptop1.setBrand("Dell");
        laptop1.setModel("abcd");
        laptop1.setPrice("100000");

        StringWriter stringWriter=new StringWriter();

        objectMapper.writeValue(stringWriter,laptop1);
        String serializedJson= stringWriter.toString();
        System.out.println(serializedJson);
    }
}

