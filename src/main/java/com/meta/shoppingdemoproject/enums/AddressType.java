package com.meta.shoppingdemoproject.enums;

import lombok.Getter;

@Getter
public enum AddressType {
    PERMANENT,TEMPORARY
}
