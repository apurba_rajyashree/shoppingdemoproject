package com.meta.shoppingdemoproject.enums;

public enum Payment {

    CASH("CASH", "Cash", "01"),
    CREDIT_CARD("CREDIT_CARD", "Credit Card", "101"),
    DEBIT_CARD("DEBIT_CARD", "Debit Card", "299");

    private String key;
    private String name;
    private String code;

    Payment(String key, String name, String code) {
        this.key = key;
        this.name = name;
        this.code = code;
    }
}
