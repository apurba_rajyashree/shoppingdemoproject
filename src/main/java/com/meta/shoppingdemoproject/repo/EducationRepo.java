package com.meta.shoppingdemoproject.repo;

import com.meta.shoppingdemoproject.model.EducationQualification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EducationRepo extends JpaRepository<EducationQualification, Integer> {
}
