package com.meta.shoppingdemoproject.repo;

import com.meta.shoppingdemoproject.model.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentRepo extends JpaRepository<Document, Integer> {

    @Query(value = "select * from document where uuid=?1", nativeQuery = true)
    Document findByUuid(String uuid);
}
