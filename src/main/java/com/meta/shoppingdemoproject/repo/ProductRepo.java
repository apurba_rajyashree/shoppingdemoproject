package com.meta.shoppingdemoproject.repo;

import com.meta.shoppingdemoproject.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepo extends JpaRepository<Product, Integer> {

    @Query(value = "select * from product where id=?1",nativeQuery = true)
    Product findByProductId(Integer id);
}
