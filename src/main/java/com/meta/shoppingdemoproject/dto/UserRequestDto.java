package com.meta.shoppingdemoproject.dto;

import com.meta.shoppingdemoproject.constants.ErrorConstants;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserRequestDto {


    private Integer id;

    @NotNull(message = ErrorConstants.NOT_NULL)
    @NotBlank(message = "User location cannot be blank")
    private String location;

    @Pattern(regexp = "[9][0-9]{9}")
    private String phoneNumber;

    @Pattern(regexp = "[a-zA-Z0-9\\-$]+@[a-z]+.[a-z]{2,3}")    //apurba.Yhapa54545@gmail.com
    private String username;

    @Size(max = 50, min = 10, message = "password should have length in between 10 to 50")
    private String password;

    public UserRequestDto(Integer id, String location, String phoneNumber, String username, String password) {
        this.id = id;
        this.location = location;
        this.phoneNumber = phoneNumber;
        this.username = username;
        this.password = password;
    }

//    @Max(value = 50, message = "Maximum length of age should be 50")
//    private Integer age;
//
//    @NotEmpty(message = "sports cannot be empty")
//    private List<String> sports;



//    @NotEmpty(message = "User cannot have empty orderList")
//    private List<Order> orderList;
//
//    private Order order;

    /***
     * [abc]   --> a,b,or c
     * [^abc]   --> any character except a,b,c
     * [a-z]  --> any character from a to z
     * [A-Z]  --> any character from A to Z
     * [a-zA-Z]   -->any charater from a to z and A to Z
     * [0-9]   --> any digit from 0 to 9
     *
     *
     *
     *  #Quantifiers (repetations)
     *  [ ]?   --> occurs 0 ot 1 time
     *  [ ]+  --> occurs 1 or more times
     *  [ ]* --> occurs 0 or more times
     *  [ ]{n}   --> occurs n times
     *  [ ]{n,m}  --> occurs atleast n and less than m
     *  [ ]{n,}  --> occurs atleast n time and more times
     *
     *
     *  #MetaCharacters
     *  \d   ---> [0-9]
     *  \D   ---> [^0-9]
     *  \w  ----> [a-zA-Z0-9]
     *  \W  ---> [^a-zA-Z0-9]
     *
     *
     *  _ , . , $ , #
     */
}
