package com.meta.shoppingdemoproject.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserGeneralInformationDto {
    private Integer id;

    @NotNull
    @NotBlank
    private String userName;
    private String email;
    private String contactNumber;
    private String password;
}
