package com.meta.shoppingdemoproject.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CollegeDto {

    private String name;

    private String email;
    private String address;
    private String contactNumber;

}
