package com.meta.shoppingdemoproject.dto;

import com.meta.shoppingdemoproject.model.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {

    private Integer id;
    private String name;
    private String description;
    private Double price;
    private Integer quantity;

    private Integer userId;


    public static void main(String[] args) {
        ProductDto productDto=new ProductDto();
        productDto.setId(65);
        productDto.setName("test_1ed587c11041");
        productDto.setDescription("test_1c7dd23a7158");
        productDto.setPrice(34.58);
        productDto.setQuantity(87);

        /**
         * {
         *   "id": 65,
         *   "name": "test_1ed587c11041",
         *   "description": "test_1c7dd23a7158",
         *   "price": 34.58,
         *   "quantity": 87,
         *   "code":"88888"
         * }
         */
    }


    /**
     * Serialize and Deserialize:
     * Serialize: Java object to Json convert
     * Deserialize: Json String/Object to Java object
     */
}

