package com.meta.shoppingdemoproject.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserFormDto {

    @NotNull
    private @Valid UserGeneralInformationDto userGeneralInformationDto;

    @NotEmpty
    private List<@Valid UserAddressDto> userAddressDtoList;

    @NotNull
    private @Valid UserEducationDto userEducationDto;
}


/**
 {
 "userGeneralInformationDto": {
 "id": null,
 "userName": "apurbarajyashreethapa",
 "email": "apurba@gmail.com",
 "contactNumber": "9876543900",
 "password":"admin@12345"
 },
 "userAddressDtoList": [
 {
 "id": null,
 "addressType": "PERMANENT",
 "street": "boudha",
 "houseNumber": "6543"
 },
 {
 "id": null,
 "addressType": "TEMPORARY",
 "street": "chabahil",
 "houseNumber": "8766"
 },
 {
 "id": null,
 "addressType": "TEMPORARY",
 "street": "sundhara",
 "houseNumber": "0976"
 }
 ],
 "userEducationDto": {
 "id": null,
 "degree": "bachelors",
 "collegeName": "texas",
 "collegeEmail": "texas@gmail.com"
 }
 }
 */
