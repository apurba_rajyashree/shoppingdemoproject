package com.meta.shoppingdemoproject.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GlobalAPIResponse {
    private Boolean status;    // true or false
    private String message;   // success or failure message
    private Object data;  //
}
