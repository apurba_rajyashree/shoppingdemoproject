package com.meta.shoppingdemoproject.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserEducationDto {
    private Integer id;
    private Integer userId;

    @NotBlank
    private String degree;
    private CollegeDto collegeDto;
}
