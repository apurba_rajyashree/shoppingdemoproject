package com.meta.shoppingdemoproject.dto;

import com.meta.shoppingdemoproject.model.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseDto {

    private Integer id;
    private String location;
    private String contactNumber;
    private String username;


    public UserResponseDto(User user) {
        this.id = user.getId();
//        this.location = user.getAddress();
        this.contactNumber = user.getContactNumber();
        this.username = user.getUsername();
    }
}
