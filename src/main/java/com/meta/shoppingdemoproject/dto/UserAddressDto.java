package com.meta.shoppingdemoproject.dto;

import com.meta.shoppingdemoproject.enums.AddressType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class UserAddressDto {

    private Integer id;
    private AddressType addressType;
    private String street;
    private String houseNumber;
}
